import { Component, OnInit,Input,HostBinding } from '@angular/core';
import {juegoClass} from '../models/juegoClass.model';
@Component({
  selector: 'app-angular-juego',
  templateUrl: './angular-juego.component.html',
  styleUrls: ['./angular-juego.component.css']
})
export class AngularJuegoComponent implements OnInit {
  @Input() jugObj : juegoClass;

  @HostBinding ('attr.class') cssClass='"col-md-4"'//agrega al tag que crea el angular el atributo de clase md4
  constructor() { }

  ngOnInit(): void {
  }

}
