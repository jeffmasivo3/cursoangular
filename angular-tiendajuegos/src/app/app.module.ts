import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularJuegoComponent } from './angular-juego/angular-juego.component';
import { AngularListajuegoComponent } from './angular-listajuego/angular-listajuego.component';

@NgModule({
  declarations: [
    AppComponent,
    AngularJuegoComponent,
    AngularListajuegoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
