import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularListajuegoComponent } from './angular-listajuego.component';

describe('AngularListajuegoComponent', () => {
  let component: AngularListajuegoComponent;
  let fixture: ComponentFixture<AngularListajuegoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularListajuegoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularListajuegoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
