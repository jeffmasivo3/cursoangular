import { Component, OnInit,Input} from '@angular/core';
import { juegoClass } from '../models/juegoClass.model';

@Component({
  selector: 'app-angular-listajuego',
  templateUrl: './angular-listajuego.component.html',
  styleUrls: ['./angular-listajuego.component.css']
})
export class AngularListajuegoComponent implements OnInit {

  juegos : juegoClass [];
  tipojuegos:string [];
  @Input() tipoj:string;
  constructor() { 
    this.juegos =[];
    this.tipojuegos=['accion','aventura','terror'];
  }
 

  ngOnInit(): void {
  }
  guardar(nombre:string,descripcion:string,tipo:string,fecha:string,costo:number):boolean 
  {
     
     this.juegos.push(new juegoClass(nombre,descripcion,tipo,fecha,costo));
     //console.log(this.juegos);
     return(false);
  }
}
