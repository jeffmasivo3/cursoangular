import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularJuegoComponent } from './angular-juego.component';

describe('AngularJuegoComponent', () => {
  let component: AngularJuegoComponent;
  let fixture: ComponentFixture<AngularJuegoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularJuegoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularJuegoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
