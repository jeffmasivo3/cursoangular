import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Destinoviaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-destinoviaje',
  templateUrl: './destinoviaje.component.html',
  styleUrls: ['./destinoviaje.component.css']
})
export class DestinoviajeComponent implements OnInit {



@Input() desObj : Destinoviaje;//variables expuesta al componente padre

@HostBinding ('attr.class') cssClass='"col-md-4"'//agrega al tag que crea el angular el atributo de clase md4
//emision de evento debo crear un metodo para el evento y crear un eventemitter
@Output() clicked:EventEmitter<Destinoviaje>// emisor del evento click,valor de salida output

constructor() { 
     this.clicked=new EventEmitter();
  }

  ngOnInit(): void {
  }
  ir(){
    this.clicked.emit(this.desObj);
    return false;
  }
}
