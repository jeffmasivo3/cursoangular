import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoviajeComponent } from './destinoviaje/destinoviaje.component';
import { ListadestinosComponent } from './listadestinos/listadestinos.component';
import { AngularJuegoComponent } from './angular-juego/angular-juego.component';

@NgModule({
  declarations: [
    AppComponent,
    DestinoviajeComponent,
    ListadestinosComponent,
    AngularJuegoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
