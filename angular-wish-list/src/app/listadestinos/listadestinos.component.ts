import { Component, OnInit} from '@angular/core';
import {Destinoviaje} from './../models/destino-viaje.model';
@Component({
  selector: 'app-listadestinos',
  templateUrl: './listadestinos.component.html',
  styleUrls: ['./listadestinos.component.css']
})

export class ListadestinosComponent implements OnInit {

	destinos : Destinoviaje []

  constructor() { 
  	this.destinos =[];
  }

  ngOnInit(): void {

  }
  guardar(nombre:string , url:string):boolean 
  {
    this.destinos.push(new Destinoviaje(nombre,url));
    console.log(this.destinos);
     return(false);
  }
}
